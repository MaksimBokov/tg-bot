from aiogram import Bot, Dispatcher, types
from dotenv import load_dotenv
import os
from aiogram.contrib.fsm_storage.memory import MemoryStorage

load_dotenv()  #функция библиотеки python-dotenv, которая загружает переменные окружения из файла .env в текущее окружение
TOKEN_tg = os.getenv('TOKEN_tg')
KEY_weather = os.getenv('KEY_weather')
KEY_info_city = os.getenv('KEY_info_city')
KEY_currency = os.getenv('KEY_currency')
KEY_nasa = os.getenv('KEY_nasa')

storage = MemoryStorage() # Это модуль aiogram, представляющий хранилище данных в оперативной памяти
bot = Bot(TOKEN_tg) # Создаем экземпляр класса Вот
dp = Dispatcher(bot, storage=storage) # Создаем экземпляр класса Диспетче





