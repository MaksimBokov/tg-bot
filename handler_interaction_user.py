from aiogram import types
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from db_SQLite import create_profil, edit_profil
from keyboard import rkm_interaction_user_create, rkm_interaction_user_cancel, rkm
from bot import dp, bot

class Bot_state(StatesGroup):
    photo = State()
    name = State()
    age = State()
    discription = State()

@dp.message_handler(commands=['complete_questionnaire'])
async def create_cmd(message : types.Message):
    await create_profil(message.from_user.id)
    await message.answer('Давай создадим профиль. Отправь мне свою фотографию',
                         reply_markup=rkm_interaction_user_cancel)
    await Bot_state.photo.set() # перевели бот из состояния None в состояние ожидания фото от пользоватля
@dp.message_handler(commands=['cancel'], state='*') # Обработчик команды / cancel
async def cancel_foo(message : types.Message, state : FSMContext):
    if state is None:
        return
    await message.answer('Вы прервали создание анкеты',
                         reply_markup=rkm_interaction_user_create)
    await state.finish()
@dp.message_handler(lambda message : not message.photo, state=Bot_state.photo) # Проверка, отправил ли нам пользователь фото
async def check_photo(message : types.Message):
    await message.reply('Это не фотография!')

@dp.message_handler(content_types=['photo'], state=Bot_state.photo) # Хендлер для обработки фото в состоянии ожидания фото от пользователя
async def load_photo(message : types.Message, state : FSMContext):
    photo = message.photo[-1]  # Получение самой крупной версии фото
    photo_file = await bot.get_file(photo.file_id)  # Получение объекта файла
    await photo_file.download(f'{photo.file_id}.jpg')  # Загрузка файла
    await message.reply('Картинка сохранена')
    async with state.proxy() as data: # Менеджер контекста для открытия локального хранилища данных (фото имя возраст описания)
        data['photo'] = message.photo[0].file_id # создали словарь, ключ - photo, значение - id фотки, которое генерируется самим API ТГ
    await message.reply('Отправь свое имя')
    await Bot_state.next() # Меняем состояние на следующее


@dp.message_handler(state=Bot_state.name) # Хендлер для обработки имени в состоянии ожидания имени от пользователя
async def load_name(message : types.Message, state : FSMContext):
    async with state.proxy() as data: # Менеджер контекста для открытия локального хранилища данных (фото имя возраст описания)
        data['name'] = message.text # создали словарь, ключ - name, значение - имя, которое написал пользователь
    await message.reply('Сколько тебе лет?')
    await Bot_state.next() # Меняем состояние на следующее

@dp.message_handler(lambda message : not message.text.isdigit() or float(message.text) > 100, state=Bot_state.age) # Проверка, отправил ли нам пользователь корректный возраст
async def check_photo(message : types.Message):
    await message.reply('Введи корректный возраст!')

@dp.message_handler(state=Bot_state.age) # Хендлер для обработки возраста в состоянии ожидания возраста от пользователя
async def load_age(message : types.Message, state : FSMContext):
    async with state.proxy() as data: # Менеджер контекста для открытия локального хранилища данных (фото имя возраст описания)
        data['age'] = message.text # создали словарь, ключ - age, значение - возраст, которое написал пользователь
    await message.reply('А теперь расскажи немного о себе')
    await Bot_state.next() # Меняем состояние на следующее

@dp.message_handler(state=Bot_state.discription) # Хендлер для обработки описания в состоянии ожидания  описания от пользователя
async def load_discription(message : types.Message, state : FSMContext):
    async with state.proxy() as data: # Менеджер контекста для открытия локального хранилища данных (фото имя возраст описания)
        data['discription'] = message.text # создали словарь, ключ - discription, значение - описание себя, которое написал пользователь
    await edit_profil(state, user_id=message.from_user.id) # записали в бд данные (именно в этой функции, тк она последняя и все данные в data уже есть)
    await message.reply('✅Ваша анкета создана!✅', reply_markup=rkm)
    await state.finish() # завершаем состояние



