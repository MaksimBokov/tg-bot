from aiogram import executor
from bot import dp
from handler import start_on


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=start_on)