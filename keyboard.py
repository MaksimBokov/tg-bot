from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove

rkm = ReplyKeyboardMarkup(resize_keyboard=True) #  Создаем клавиатуру
kb3 = KeyboardButton('/animals') # Создаем кнопки
kb4 = KeyboardButton('/weather')
kb5 = KeyboardButton('/currency')
kb6 = KeyboardButton('/photo_nasa')
kb9 = KeyboardButton('/translate')
kb10 = KeyboardButton('/complete_questionnaire')
kb11 = KeyboardButton('/info_user')
kb12 = KeyboardButton('/photo_user')
rkm.add(kb5, kb3, kb4, kb6, kb9, kb10, kb11, kb12) # Добавляем кнопки в клавиатур

Inline_Keyboard_info = InlineKeyboardMarkup(row_width=9)# Создаем инлайн клавиатуру
Inline_Button = InlineKeyboardButton(text='✅Немного INFO о городе✅', callback_data='info_city') # Создаем кнопку
Inline_Keyboard_info.add(Inline_Button)# Добавляем кнопки в инлайн клавиатуру

Inline_Keyboard_site = InlineKeyboardMarkup(row_width=2)
Inline_Button_1 = InlineKeyboardButton(text='🌏Атлас', url='https://xn--24-6kct3an.xn--p1ai/%D0%93%D0%B5%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F_%D0%90%D1%82%D0%BB%D0%B0%D1%81_7_%D0%BA%D0%BB%D0%B0%D1%81%D1%81/index.html')
Inline_Button_2 = InlineKeyboardButton(text='📖Словарь Ожигова', url='https://slovarozhegova.ru/')
Inline_Keyboard_site.add(Inline_Button_1, Inline_Button_2)

Inline_Keyboard_weather = InlineKeyboardMarkup(row_width=2)
Inline_Button_3 = InlineKeyboardButton('🆕НОВЫЙ ЗАПРОС🆕', callback_data='new')
Inline_Button_4 = InlineKeyboardButton('✅ГЛАВНОЕ МЕНЮ✅', callback_data='menu')
Inline_Keyboard_weather.add(Inline_Button_3,Inline_Button_4)


rkm_interaction_user_create = ReplyKeyboardMarkup(resize_keyboard=True)
rkm_interaction_user_create.add(KeyboardButton('/create'))

rkm_interaction_user_cancel = ReplyKeyboardMarkup(resize_keyboard=True)
rkm_interaction_user_cancel.add(KeyboardButton('/cancel'))
