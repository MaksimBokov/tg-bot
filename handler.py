from bot import dp, bot, KEY_weather, KEY_info_city, KEY_currency, types, KEY_nasa
from keyboard import rkm, Inline_Keyboard_info, Inline_Keyboard_site, Inline_Keyboard_weather
import requests
import json
from aiogram.dispatcher.filters.state import State, StatesGroup
from db_SQLite import db_start, select
from handler_interaction_user import create_cmd, load_photo
import cv2
import os


COMMANDS = """
<b>/animals</b> - <em>🐶Милое фото собаки🐶</em>
<b>/weather</b> - <em>🌤Текущая погода🌤</em>
<b>/currency</b> - <em>💵Конвертер валют💵</em>
<b>/photo_nasa</b> - <em>🪐Фото дня от NASA🪐</em>
<b>/translate</b> - <em>Переводчик с 🇬🇧 на 🇷🇺</em>
<b>/complete_questionnaire</b> - <em>Заполить анкету✍️</em>
<b>/info_user</b> - <em>🥸Получить данные о себе🥸</em>
<b>/photo_user</b> - <em>😶Определение лица😶</em>
"""
List_currencies = """
<b>/USDRUB1</b> - <em>1️⃣ доллар в рублях 💵</em>
<b>/EURRUB1</b> - <em>1️⃣ евро в рублях 💶</em>
"""

class MachineState(StatesGroup): # Класс, который создает группу состояний с именем city  conversion text
    city = State()
    conversion = State()
    text1 = State()


async def start_on(message): # показывает в консоли что бот запущен и подключает бд
    await db_start()
    print('Бот запущен!!!')

@dp.message_handler(commands=['info_user']) # просмотр инфы о себе, которую пользователь записал в анкете
async def select_user(message : types.Message):
    result = await select(message.from_user.id)
    if result:
        await message.answer(f'✅user_id - {result[0]} \n'
                             f'✅имя - {result[2]} \n'
                             f'✅возраст - {result[3]} \n'
                             f'✅инфа - {result[4]}')
        await bot.send_photo(chat_id=message.from_user.id,
                             photo=result[1])
    else:
        await message.answer('❗️Анкета еще не создавалась❗️')

@dp.message_handler(commands=['photo_user']) # определение лица на фотo

async def user(message : types.Message):
    my_photo = ''
    print(os.listdir(os.getcwd()))
    for i in os.listdir(os.getcwd()):
        if i.endswith('.jpg'):
            my_photo = i
            break
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    img = cv2.imread(my_photo)
    gray = cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 3)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    cv2.imwrite('detected_faces.jpg', img)
    with open('detected_faces.jpg', 'rb') as photo: # Отправка изображения с обнаруженными лицами пользователю
        await message.reply_photo(photo)
    os.remove(my_photo)
    os.remove('detected_faces.jpg')


@dp.message_handler(commands=['complete_questionnaire']) # обработчик команды complete_questionnaire
async def create_cmd(message : types.Message):
    await create_cmd()

@dp.message_handler(commands=['translate']) # обработчик команды translate
async def com_text(message):
    await bot.send_message(chat_id=message.from_user.id, text='Введите текст на английском 🇬🇧 :')
    await MachineState.text1.set()  # устанавливаем состояние на "ввод текста"

@dp.message_handler(state=MachineState.text1)
async def transl(message, state):
    try:
        text_trans = message.text
        url = "https://google-translate1.p.rapidapi.com/language/translate/v2"
        payload = {
            "q": f"{text_trans}",
            "target": "ru",
            "source": "en"
        }
        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "Accept-Encoding": "application/gzip",
            "X-RapidAPI-Key": "f1b210ad42msh53cbd8262538203p1e1d7fjsnefcb6d785928",
            "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
        }
        response = requests.post(url, data=payload, headers=headers)
        await message.answer(response.json()['data']['translations'][0]['translatedText'])
    except BaseException:
        await message.answer('Непредвиденная ошибка')
    finally:
        await state.finish()

@dp.message_handler(commands=['photo_nasa']) # обработчик команды photo_nasa
async def com_nasa(message):
    try:
        url = f'https://api.nasa.gov/planetary/apod?api_key={KEY_nasa}'
        data = requests.get(url)
        await bot.send_photo(chat_id=message.from_user.id,
                                photo=data.json()['url'])
    except:
        await message.answer('Извините, сервис временно не доступен ☹️')



@dp.message_handler(commands=['start']) # обработчик команды start
async def com_start(message):
    await bot.send_sticker(chat_id=message.from_user.id,
                           sticker='CAACAgIAAxkBAAEI_btkYvt8v8fQ3JTFwkIUjqTUchcv0QACSwADUomRI8htaM0_6NrrLwQ')
    await bot.send_message(chat_id=message.from_user.id,
                           text=f'🙌 Привет, <b>{message.from_user.first_name}</b> '
                                f'\n <em>Вот список команд, нажимай любую</em> '
                                f'\n{COMMANDS}',
                           parse_mode='HTML',
                           reply_markup=rkm)

@dp.message_handler(commands=['animals']) # обработчик команды animals
async def com_animals(message):
    source = requests.get('https://dog.ceo/api/breeds/image/random')
    data = json.loads(source.text)
    image = data['message']
    await bot.send_photo(chat_id=message.from_user.id,
                           photo=image)



@dp.message_handler(commands=['weather']) # обработчик команды weather
async def com_weathery(message):
    await bot.send_message(chat_id=message.from_user.id, text='Введите название города:')
    await MachineState.city.set()  # устанавливаем состояние на "ввод города"

def checking_sign(data):
    if data > 0:
        return '➕'
    elif data < 0:
        return ''


@dp.message_handler(state=MachineState.city)  # обработчик в состоянии "ввод города"
async def set_city(message, state):
    try: # обработка исключений
        name_city = message.text
        url = f"https://api.openweathermap.org/data/2.5/weather?q={name_city}&appid={KEY_weather}&units=metric"
        global response_city
        response_weather = requests.get(url)
        response_city = response_weather.json()['name']
        data = response_weather.json()['main']['temp']
        global latitude
        global longitude
        latitude = response_weather.json()['coord']['lat']
        longitude = response_weather.json()['coord']['lon']
        await message.answer(
            f'Текущая температура 🌎 '
            f'<b>{name_city}</b> '
            f'{checking_sign(data)}<b>{round(data)}</b>',
            parse_mode='HTML', reply_markup=Inline_Keyboard_info)

    except KeyError:
        await message.answer('☹️Город не найден. Проверь орфографию (или географию😉)', reply_markup=Inline_Keyboard_site)
        await message.answer(f'Выбери действие:', parse_mode='HTML', reply_markup=Inline_Keyboard_weather)
    finally:
        await state.finish()  # завершаем состояние после ответа

@dp.callback_query_handler()  # Обработчик калбэк запроса телеграма в ответ на возникшее событие
async def callback_info(callback):
    try:
        if callback.data == 'info_city':
            api_url = f'https://api.api-ninjas.com/v1/city?name={response_city}'
            response = requests.get(api_url, headers={'X-Api-Key': KEY_info_city})
            if response.status_code == requests.codes.ok:
                info_population, info_country = response.json()[0]['population'], response.json()[0]['country']
                await bot.send_message(chat_id=callback.from_user.id, text=f'Население - {info_population} 🧍‍♂️ \nСтрана - {info_country} 🏙')
                await bot.send_location(chat_id=callback.from_user.id, latitude=latitude, longitude=longitude, reply_markup=rkm)

            else:
                await callback.answer(text=response.status_code)
        elif callback.data == 'new':
            await com_weathery(callback)
        elif callback.data == 'menu':
            await bot.send_message(chat_id=callback.from_user.id, text=COMMANDS, parse_mode='HTML')
    except:
        await bot.send_message(chat_id=callback.from_user.id, text='😑Информация не найдена😑', reply_markup=rkm)

@dp.message_handler(commands=['currency'])
async def com_currency(message):
    await message.answer(f'💰Нужна валютная пара и количество в формате "USDRUB1" '
                         f'\nИли выберите популярные из списка: \n{List_currencies}',
                         parse_mode='HTML')
    await MachineState.conversion.set()

@dp.message_handler(state=MachineState.conversion)
async def conversion(message, state):
    try:
        name_currency = message.text
        url = "https://currency-conversion-and-exchange-rates.p.rapidapi.com/convert"
        querystring = {"from": {name_currency[1:4]}, "to": {name_currency[4:7]}, "amount": {name_currency[7:]}}
        headers = {
            "X-RapidAPI-Key": KEY_currency,
            "X-RapidAPI-Host": "currency-conversion-and-exchange-rates.p.rapidapi.com"
        }
        response = requests.get(url, headers=headers, params=querystring)
        await message.answer(f'{name_currency[7:]} {name_currency[1:4]} 🟰 {round(response.json()["result"], 2)} {name_currency[4:7]}',
                             reply_markup=rkm)

    except KeyError:
        await message.answer('‼️Ошибка: конвертация не прошла. \n Проверь вводимые данные‼️', reply_markup=rkm)
    finally:
        await state.finish()  # завершаем состояние после ответа



